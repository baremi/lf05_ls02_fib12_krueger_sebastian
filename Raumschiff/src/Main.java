
public class Main {
	
	public static void main(String[] args) {
	Ladung l1 = new Ladung();
	l1.setBezeichnung("Ferengi Schneckensaft");
	l1.setMenge(200);
	
	Ladung l2 = new Ladung();
	l2.setBezeichnung("Borg-Schrott");
	l2.setMenge(5);
	
	Ladung l3 = new Ladung();
	l3.setBezeichnung("Rote Materie");
	l3.setMenge(2);
	
	Ladung l4 = new Ladung();
	l4.setBezeichnung("Forschungssonde");
	l4.setMenge(35);
	
	Ladung l5 = new Ladung();
	l5.setBezeichnung("Bat'leth Klingonen Schwert");
	l5.setMenge(200);
	
	Ladung l6 = new Ladung();
	l6.setBezeichnung("Plasma-Waffe");
	l6.setMenge(50);
	
	Ladung l7 = new Ladung();
	l7.setBezeichnung("Photonentorpedo");
	l7.setMenge(3);
	
	Raumschiff klingonen = new Raumschiff();
	klingonen.setPhotonentorpedoAnzahl(1);
	klingonen.setEnergieversorgungInProzent(100);
	klingonen.setSchildeInProzent(100);
	klingonen.setHuelleInProzent(100);
	klingonen.setLebenserhaltungssystemeInProzent(100);
	klingonen.setSchiffsname("IKS Hegh'ta");
	klingonen.setAndroidenAnzahl(2);
	
	Raumschiff romulaner = new Raumschiff();
	romulaner.setPhotonentorpedoAnzahl(2);
	romulaner.setEnergieversorgungInProzent(100);
	romulaner.setSchildeInProzent(100);
	romulaner.setHuelleInProzent(100);
	romulaner.setLebenserhaltungssystemeInProzent(100);
	romulaner.setSchiffsname("IRW Khazara");
	romulaner.setAndroidenAnzahl(2);
	
	Raumschiff vulkanier = new Raumschiff();
	vulkanier.setPhotonentorpedoAnzahl(0);
	vulkanier.setEnergieversorgungInProzent(80);
	vulkanier.setSchildeInProzent(80);
	vulkanier.setHuelleInProzent(50);
	vulkanier.setLebenserhaltungssystemeInProzent(100);
	vulkanier.setSchiffsname("Ni'Var");
	vulkanier.setAndroidenAnzahl(5);
			
//	System.out.println("Ladung 1:\nName: " + l1.getBezeichnung() + "\nMenge: " + l1.getMenge());
//	System.out.println("\nLadung 2:\nName: " + l2.getBezeichnung() + "\nMenge: " + l2.getMenge());
//	System.out.println("\nLadung 3:\nName: " + l3.getBezeichnung() + "\nMenge: " + l3.getMenge());
//	System.out.println("\nLadung 4:\nName: " + l4.getBezeichnung() + "\nMenge: " + l4.getMenge());
//	System.out.println("\nLadung 5:\nName: " + l5.getBezeichnung() + "\nMenge: " + l5.getMenge());
//	System.out.println("\nLadung 6:\nName: " + l6.getBezeichnung() + "\nMenge: " + l6.getMenge());
//	System.out.println("\nLadung 7:\nName: " + l7.getBezeichnung() + "\nMenge: " + l7.getMenge());
//	
//	System.out.println("\nRaumschiff der Klasse Klingonen:\nName: " + klingonen.getSchiffsname() + "\nPhotonentorpedo Anzahl: " + klingonen.getPhotonentorpedoAnzahl() + "\nEnergieversogung in Prozent: " + klingonen.getEnergieversorgungInProzent() + "\nSchilde in Prozent: " + klingonen.getSchildeInProzent() + "\nH�lle in Prozent: " + klingonen.getHuelleInProzent() + "\nLebenserhaltungssysteme in Prozent: " + klingonen.getLebenserhaltungssystemeInProzent() + "\nAndroidenanzahl: " + klingonen.getAndroidenAnzahl());
//	System.out.println("\nRaumschiff der Klasse Romulaner:\nName: " + romulaner.getSchiffsname() + "\nPhotonentorpedo Anzahl: " + romulaner.getPhotonentorpedoAnzahl() + "\nEnergieversogung in Prozent: " + romulaner.getEnergieversorgungInProzent() + "\nSchilde in Prozent: " + romulaner.getSchildeInProzent() + "\nH�lle in Prozent: " + romulaner.getHuelleInProzent() + "\nLebenserhaltungssysteme in Prozent: " + romulaner.getLebenserhaltungssystemeInProzent() + "\nAndroidenanzahl: " + romulaner.getAndroidenAnzahl());
//	System.out.println("\nRaumschiff der Klasse Vulkanier:\nName: " + vulkanier.getSchiffsname() + "\nPhotonentorpedo Anzahl: " + vulkanier.getPhotonentorpedoAnzahl() + "\nEnergieversogung in Prozent: " + vulkanier.getEnergieversorgungInProzent() + "\nSchilde in Prozent: " + vulkanier.getSchildeInProzent() + "\nH�lle in Prozent: " + vulkanier.getHuelleInProzent() + "\nLebenserhaltungssysteme in Prozent: " + vulkanier.getLebenserhaltungssystemeInProzent() + "\nAndroidenanzahl: " + vulkanier.getAndroidenAnzahl());
//	
//	System.out.println("\nLadung des Raumschiffs der Klasse Klingonen (" + klingonen.getSchiffsname() + "): " + l1.getMenge() + "x " + l1.getBezeichnung() + " und " + l5.getMenge() + "x " + l5.getBezeichnung());
//	System.out.println("\nLadung des Raumschiffs der Klasse Romulaner (" + romulaner.getSchiffsname() + "): " + l2.getMenge() + "x " + l2.getBezeichnung() + ", " + l3.getMenge() + "x " + l3.getBezeichnung() + " und " + l6.getMenge() + "x " + l6.getBezeichnung());
//	System.out.println("\nLadung des Raumschiffs der Klasse Vulkanier (" + vulkanier.getSchiffsname() + "): " + l4.getMenge() + "x " + l4.getBezeichnung() + " und " + l7.getMenge() + "x " + l7.getBezeichnung());
	
	// Szenario Anfang
	
	while(klingonen.getPhotonentorpedoAnzahl() != 0)
	{
		System.out.println("\nAktuelle Photonentorpedoanzahl: " + klingonen.getPhotonentorpedoAnzahl());
		klingonen.photonentorpedoSchiessen(klingonen);
		System.out.println("Photonentorpedo abgeschossen " + "\nNeue Photonentorpedoanzahl: " + klingonen.getPhotonentorpedoAnzahl());
		l7.setMenge(2);
		klingonen.treffer(romulaner);
		klingonen.nachrichtAnAlle("");
	}
	
	while(romulaner.getEnergieversorgungInProzent() > 50)
	{
		System.out.println("\nAktuelle Energieversorgung in Prozent: " + romulaner.getEnergieversorgungInProzent());
		romulaner.phaserkanoneSchiessen(romulaner);
		System.out.println("Phaserkanonen abgeschossen" + "\nNeue Energieversorgung in Prozent: " + romulaner.getEnergieversorgungInProzent());
		romulaner.nachrichtAnAlle("");
		romulaner.treffer(klingonen);
	}
	
	System.out.println("\nGewalt ist nicht logisch!");
	System.out.println("\nRaumschiff der Klasse Klingonen:\nName: " + klingonen.getSchiffsname() + "\nPhotonentorpedo Anzahl: " + klingonen.getPhotonentorpedoAnzahl() + "\nEnergieversogung in Prozent: " + klingonen.getEnergieversorgungInProzent() + "\nSchilde in Prozent: " + klingonen.getSchildeInProzent() + "\nH�lle in Prozent: " + klingonen.getHuelleInProzent() + "\nLebenserhaltungssysteme in Prozent: " + klingonen.getLebenserhaltungssystemeInProzent() + "\nAndroidenanzahl: " + klingonen.getAndroidenAnzahl());
	System.out.println("\nLadung des Raumschiffs der Klasse Klingonen (" + klingonen.getSchiffsname() + "): " + l1.getMenge() + "x " + l1.getBezeichnung() + " und " + l5.getMenge() + "x " + l5.getBezeichnung());
	
	klingonen.photonentorpedosLaden(l7.getMenge());
//	klingonen.setPhotonentorpedoAnzahl(l7.getMenge() -1);
	System.out.println("\nPhotonentorpedos aufgef�llt und abschussbereit!");
	
	while(klingonen.getPhotonentorpedoAnzahl() != 0)
	{
		System.out.println("\nAktuelle Photonentorpedoanzahl: " + klingonen.getPhotonentorpedoAnzahl());
		klingonen.photonentorpedoSchiessen(klingonen);
		System.out.println("Photonentorpedo abgeschossen " + "\nNeue Photonentorpedoanzahl: " + klingonen.getPhotonentorpedoAnzahl());
		klingonen.treffer(romulaner);
		klingonen.nachrichtAnAlle("");
	}
	
	
	System.out.println("\nRaumschiff der Klasse Klingonen:\nName: " + klingonen.getSchiffsname() + "\nPhotonentorpedo Anzahl: " + klingonen.getPhotonentorpedoAnzahl() + "\nEnergieversogung in Prozent: " + klingonen.getEnergieversorgungInProzent() + "\nSchilde in Prozent: " + klingonen.getSchildeInProzent() + "\nH�lle in Prozent: " + klingonen.getHuelleInProzent() + "\nLebenserhaltungssysteme in Prozent: " + klingonen.getLebenserhaltungssystemeInProzent() + "\nAndroidenanzahl: " + klingonen.getAndroidenAnzahl());
	System.out.println("\nRaumschiff der Klasse Romulaner:\nName: " + romulaner.getSchiffsname() + "\nPhotonentorpedo Anzahl: " + romulaner.getPhotonentorpedoAnzahl() + "\nEnergieversogung in Prozent: " + romulaner.getEnergieversorgungInProzent() + "\nSchilde in Prozent: " + romulaner.getSchildeInProzent() + "\nH�lle in Prozent: " + romulaner.getHuelleInProzent() + "\nLebenserhaltungssysteme in Prozent: " + romulaner.getLebenserhaltungssystemeInProzent() + "\nAndroidenanzahl: " + romulaner.getAndroidenAnzahl());
	System.out.println("\nRaumschiff der Klasse Vulkanier:\nName: " + vulkanier.getSchiffsname() + "\nPhotonentorpedo Anzahl: " + vulkanier.getPhotonentorpedoAnzahl() + "\nEnergieversogung in Prozent: " + vulkanier.getEnergieversorgungInProzent() + "\nSchilde in Prozent: " + vulkanier.getSchildeInProzent() + "\nH�lle in Prozent: " + vulkanier.getHuelleInProzent() + "\nLebenserhaltungssysteme in Prozent: " + vulkanier.getLebenserhaltungssystemeInProzent() + "\nAndroidenanzahl: " + vulkanier.getAndroidenAnzahl());
	
	System.out.println("\nLadung des Raumschiffs der Klasse Klingonen (" + klingonen.getSchiffsname() + "): " + l1.getMenge() + "x " + l1.getBezeichnung() + " und " + l5.getMenge() + "x " + l5.getBezeichnung());
	System.out.println("\nLadung des Raumschiffs der Klasse Romulaner (" + romulaner.getSchiffsname() + "): " + l2.getMenge() + "x " + l2.getBezeichnung() + ", " + l3.getMenge() + "x " + l3.getBezeichnung() + " und " + l6.getMenge() + "x " + l6.getBezeichnung());
	System.out.println("\nLadung des Raumschiffs der Klasse Vulkanier (" + vulkanier.getSchiffsname() + "): " + l4.getMenge() + "x " + l4.getBezeichnung() + " und " + l7.getMenge() + "x " + l7.getBezeichnung());
	
	
	}
	
	
}
