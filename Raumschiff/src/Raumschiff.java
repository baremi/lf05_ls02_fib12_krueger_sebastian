
public class Raumschiff {
	
	private int photonentorpedoAnzahl;
	private int energieversorgungInProzent;
	private int schildeInProzent;
	private int huelleInProzent;
	private int lebenserhaltungssystemeInProzent;
	private int androidenAnzahl;
	private String schiffsname;
	
	private String broadcastKommunikator[];
	private Ladung ladungsverzeichnis[];
	
	

	public int getPhotonentorpedoAnzahl() {
		return photonentorpedoAnzahl;
	}
	public void setPhotonentorpedoAnzahl(int photonentorpedoAnzahl) {
		this.photonentorpedoAnzahl = photonentorpedoAnzahl;
	}
	public int getEnergieversorgungInProzent() {
		return energieversorgungInProzent;
	}
	public void setEnergieversorgungInProzent(int energieversorgungInProzent) {
		this.energieversorgungInProzent = energieversorgungInProzent;
	}
	public int getSchildeInProzent() {
		return schildeInProzent;
	}
	public void setSchildeInProzent(int schildeInProzent) {
		this.schildeInProzent = schildeInProzent;
	}
	public int getHuelleInProzent() {
		return huelleInProzent;
	}
	public void setHuelleInProzent(int huelleInProzent) {
		this.huelleInProzent = huelleInProzent;
	}
	public int getLebenserhaltungssystemeInProzent() {
		return lebenserhaltungssystemeInProzent;
	}
	public void setLebenserhaltungssystemeInProzent(int lebenserhaltungssystemeInProzent) {
		this.lebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProzent;
	}
	public int getAndroidenAnzahl() {
		return androidenAnzahl;
	}
	public void setAndroidenAnzahl(int androidenAnzahl) {
		this.androidenAnzahl = androidenAnzahl;
	}
	public String getSchiffsname() {
		return schiffsname;
	}
	public void setSchiffsname(String schiffsname) {
		this.schiffsname = schiffsname;
	}
	
	public void Raumschiff()
	{

	}

	public void addLadung(Ladung neueLadung)
	{
		neueLadung = new Ladung();
	}

	public void photonentorpedoSchiessen(Raumschiff r)
	{
		r.photonentorpedoAnzahl = photonentorpedoAnzahl -1;
	}

	public void phaserkanoneSchiessen(Raumschiff r)
	{
		r.energieversorgungInProzent = energieversorgungInProzent -50;
	}

	public void treffer(Raumschiff r)
	{
		System.out.println(schiffsname + " wurde getroffen!");
		schildeInProzent = schildeInProzent -50;
		if(schildeInProzent == 0)
		{
			huelleInProzent = huelleInProzent -50;
			energieversorgungInProzent = energieversorgungInProzent -50;
		}
	}

	public void nachrichtAnAlle(String message)
	{
		System.out.println("-=*CLICK*=-");
		
	}
	
	public String eintraegeLogbuchZurueckgeben[];

	public void photonentorpedosLaden(int anzahlTorpedos)
	{
		photonentorpedoAnzahl = anzahlTorpedos;
	}

	public void reperaturDurchfuehren()
	{

	}	

	public void zustandRaumschiff()
	{
		
	}

	public void ladungsverzeichnisAusgeben()
	{
		
	}

	public void ladungsverzeichnisAufraeumen()
	{

	}

	public static void main(String[] args) {
		
		
		

	}

}
